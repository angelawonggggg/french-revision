import Login from "../components/Login";
import NavBar from "../components/NavBar";

export default function () {
  return (
    <div>
      <NavBar />
      <Login />
    </div>
  );
}
