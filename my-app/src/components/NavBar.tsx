import "./NavBar.css";

export default function NavBar() {
  return (
    <div className="navBar">
     
      <div className="tabs">
        <div className="page">Home</div>
        <div className="page">To Do List</div>
      </div>
    </div>
  );
}
